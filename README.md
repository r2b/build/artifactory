# Scripts

Este proyecto contiene scripts utilizados en los diferentes proyectos JVM


### Ejemplo Kotlin
1) Agregar las dependencias al buildscript
        classpath "org.jfrog.buildinfo:build-info-extractor-gradle:4.7.4"
        classpath "net.nemerosa:versioning:2.7.1"
``` gradle
buildscript {
    ext.kotlin_version = '1.2.60'

    repositories {
        jcenter()
        mavenCentral()
        maven {url "https://plugins.gradle.org/m2/" }
    }
    dependencies {
        ....
        classpath "org.jfrog.buildinfo:build-info-extractor-gradle:4.7.4"
        classpath "net.nemerosa:versioning:2.7.1"
        ....
    }
}
```

aplicar los plugins script necesarios

# Semantic Release

```gradle
apply from : 'https://gitlab.com/r2b/build/artifactory/raw/master/version.gradle'
```
semantic-release automates the whole package release workflow including: determining the next version number, generating the release notes and publishing the package.

apply from : 'https://gitlab.com/sign4l-e7a/cnf4-dev/raw/master/version.gradle'

Aplica este plugins si estas trabajando con Git REPO, las versiones se determinan
por las Etiquetas asociadas al commit
EJM 
```
git tag -a 1.0.0 0fb4bde -m "r1.0.0" 
```
cuando se ejecuta un commit, sin una version el script añade el sufijo SNAPSHOT
resutaldo: 
mylib-SNAPHOST.jar


# Conectando a los repositorios del proyecto

Aplicar al final del buildscript

- Android
```gradle
apply from : 'https://gitlab.com/r2b/build/artifactory/raw/master/plugin-4ado.gradle'

```
- Java
```gradle
apply from : 'https://gitlab.com/r2b/build/artifactory/raw/master/plugin-4j.gradle'
```

## Como publicar en el repositorio.
Los Artefactos admitidos
mavenJava','mavenProject','project','kotlinProject','kotlin','kotlinjvm','kotlinjs'

```gradle
apply plugin: 'maven-publish'
apply from: "$rootProject.projectDir/gradle/pom-descriptor.gradle"


configurations.all {
    resolutionStrategy.cacheChangingModulesFor 0, 'seconds'
}

publishing {
    publications {
        //mavenProject, es uno de las publicaciones
        mavenProject(MavenPublication) {
            from components.java
            groupId project.group
            artifactId project.name
            version project.version

            artifact sourceJar {
                classifier 'sources'
            }
        }
    }
}

apply from: "https://gitlab.com/r2b/build/artifactory/raw/master/plugin-4j.gradle"

 ```